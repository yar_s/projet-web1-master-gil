package fr.univrouen.projetweb.controller;

import fr.univrouen.projetweb.model.AppRole;
import fr.univrouen.projetweb.model.AppRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/roles")
public class AppRoleController {

    private final AppRoleService appRoleService;

    @Autowired
    public AppRoleController(AppRoleService appRoleService) {
        this.appRoleService = appRoleService;
    }

    @PostMapping
    public AppRole creerRole(@RequestBody AppRole appRole) {
        return appRoleService.creerRole(appRole);
    }

    @GetMapping
    public List<AppRole> obtenirTousLesRoles() {
        return appRoleService.obtenirTousLesRoles();
    }

    @GetMapping("/{id}")
    public AppRole obtenirRoleParId(@PathVariable Long id) {
        return appRoleService.obtenirRoleParId(id);
    }
    @GetMapping("/{roleName}")
    public AppRole obtenirRoleParId(@PathVariable String roleName){
        return appRoleService.obtenirRoleParNom(roleName);
    }

    @PutMapping("/{id}")
    public AppRole mettreAJourRole(@PathVariable Long id, @RequestBody AppRole appRole) {
        return appRoleService.mettreAJourRole(id, appRole);
    }

    @DeleteMapping("/{id}")
    public void supprimerRole(@PathVariable Long id) {
        appRoleService.supprimerRole(id);
    }
}
