package fr.univrouen.projetweb.controller;

import fr.univrouen.projetweb.model.AppUser;
import fr.univrouen.projetweb.model.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class AppUserController {

    private final AppUserService appUserService;

    @Autowired
    public AppUserController(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @PostMapping
    public AppUser ajouterUtilisateur(@RequestBody AppUser appUser) {
        return appUserService.ajouterUtilisateur(appUser);
    }

    @GetMapping
    //@PreAuthorize("hasAuthority('USER') and hasAuthority('ADMIN')")
    public List<AppUser> obtenirTousLesUtilisateurs() {
        return appUserService.obtenirTousLesUtilisateurs();
    }

    @GetMapping("/{username}")
    public AppUser obtenirUtilisateurParNom(@PathVariable String username) {
        return appUserService.obtenirUtilisateurParNom(username);
    }

    @PutMapping("/{id}")
    public AppUser mettreAJourUtilisateur(@PathVariable Long id, @RequestBody AppUser appUser) {
        return appUserService.mettreAJourUtilisateur(id, appUser);
    }

    @DeleteMapping("/{id}")
    public void supprimerUtilisateur(@PathVariable Long id) {
        appUserService.supprimerUtilisateur(id);
    }

    @PostMapping("/{username}/roles/{roleName}")
    public AppUser ajouterRoleAUtilisateur(@PathVariable String username, @PathVariable String roleName) {
        return appUserService.ajouterRoleAUtilisateur(username, roleName);
    }
}
