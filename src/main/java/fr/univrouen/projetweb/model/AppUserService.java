package fr.univrouen.projetweb.model;

import java.util.List;

public interface AppUserService {
    AppUser ajouterUtilisateur(AppUser appUser);
    AppUser ajouterRoleAUtilisateur(String username, String roleName);
    AppUser obtenirUtilisateurParNom(String username);
    List<AppUser> obtenirTousLesUtilisateurs();
    AppUser mettreAJourUtilisateur(Long id, AppUser appUser);
    void supprimerUtilisateur(Long id);
}
