package fr.univrouen.projetweb.model;


import org.springframework.data.jpa.repository.JpaRepository;


public interface InvalidJwtTokenRepository extends JpaRepository<InvalidJwtToken,Long> {
	
	InvalidJwtToken findByToken(String token);
	
}
