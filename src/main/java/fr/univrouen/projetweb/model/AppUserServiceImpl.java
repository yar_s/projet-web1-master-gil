package fr.univrouen.projetweb.model;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import jakarta.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class AppUserServiceImpl implements AppUserService {
    private final AppUserRepository appUserRepository;
    private final AppRoleRepository appRoleRepository;
    private final PasswordEncoder passwordEncoder;

    public AppUserServiceImpl(AppUserRepository appUserRepository,
                              AppRoleRepository appRoleRepository,
                              PasswordEncoder passwordEncoder) {
        this.appUserRepository = appUserRepository;
        this.appRoleRepository = appRoleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public AppUser ajouterUtilisateur(AppUser appUser) {
        appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
        return appUserRepository.save(appUser);
    }

    @Override
    public AppUser ajouterRoleAUtilisateur(String nomUtilisateur, String nomRole) {
        AppUser utilisateur = appUserRepository.findByUsername(nomUtilisateur);
        if (utilisateur == null) {
            throw new RuntimeException("Utilisateur introuvable");
        }
        AppRole role = appRoleRepository.findByRolename(nomRole);
        if (role == null) {
            throw new RuntimeException("Rôle introuvable");
        }
        utilisateur.getRoles().add(role);
        return appUserRepository.save(utilisateur);
    }

    @Override
    public AppUser obtenirUtilisateurParNom(String nomUtilisateur) {
        return appUserRepository.findByUsername(nomUtilisateur);
    }

    @Override
    public List<AppUser> obtenirTousLesUtilisateurs() {
        return appUserRepository.findAll();
    }

    @Override
    public AppUser mettreAJourUtilisateur(Long id, AppUser appUser) {
        return appUserRepository.findById(id)
                .map(existingUser -> {
                    existingUser.setUsername(appUser.getUsername());
                    existingUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
                    return appUserRepository.save(existingUser);
                }).orElseThrow(() -> new RuntimeException("Utilisateur introuvable"));
    }

    @Override
    public void supprimerUtilisateur(Long id) {
        if (!appUserRepository.existsById(id)) {
            throw new RuntimeException("Utilisateur introuvable");
        }
        appUserRepository.deleteById(id);
    }
}
