package fr.univrouen.projetweb.model;

import jakarta.persistence.*;
import lombok.*;
import jakarta.validation.constraints.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class AppRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "le nom du role ne peut pas être vide")
    @Size(min = 3, max = 20, message = "Le nom du rôle doit contenir entre 3 et 20 caractères")
    private String rolename;

}
