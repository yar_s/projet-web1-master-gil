package fr.univrouen.projetweb.model;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JwtBlacklistService {

    @Autowired
    private InvalidJwtTokenRepository invalidJwtTokenRepository;

    public void blacklistToken(String token) {
        InvalidJwtToken invalidJwtToken = new InvalidJwtToken();
        invalidJwtToken.setToken(token);
        invalidJwtTokenRepository.save(invalidJwtToken);
    }

    public boolean isTokenBlacklisted(String token) {
        return invalidJwtTokenRepository.findByToken(token) != null;
    }

}
