package fr.univrouen.projetweb.model;

import java.util.List;

public interface AppRoleService {
	AppRole creerRole(AppRole appRole);

	AppRole mettreAJourRole(Long id, AppRole appRole);

	void supprimerRole(Long id);

	List<AppRole> obtenirTousLesRoles();

	AppRole obtenirRoleParId(Long id);
	
	AppRole obtenirRoleParNom(String nomRole);
}
