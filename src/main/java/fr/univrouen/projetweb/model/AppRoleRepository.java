package fr.univrouen.projetweb.model;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface AppRoleRepository extends JpaRepository<AppRole,Long> {

    AppRole findByRolename(String userRole);
}
