package fr.univrouen.projetweb.model;

import jakarta.persistence.*;
import lombok.*;
import jakarta.validation.constraints.*;

import java.util.ArrayList;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "le nom d'utilisateur ne peut pas être vide")
    @Size(min = 3, max = 20, message = "Le nom d'utilisateur doit contenir entre 3 et 20 caractères")
    private String username;

    @NotBlank(message = "le mot de passe ne peut pas être vide")
    @Size(min = 6, message = "le mot de passe doit contenir au moins 6 caractères")
    private String password;

    @Email(message = "adresse email non valide")
    @NotBlank(message = "l'adresse email ne peut pas être vide")
    private String email;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Builder.Default
    private Collection<AppRole> roles = new ArrayList<>();

   
}
