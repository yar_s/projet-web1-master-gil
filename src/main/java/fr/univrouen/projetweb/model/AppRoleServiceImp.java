package fr.univrouen.projetweb.model;

import org.springframework.stereotype.Service;
import jakarta.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class AppRoleServiceImp implements AppRoleService {

    private final AppRoleRepository appRoleRepository;

    public AppRoleServiceImp(AppRoleRepository appRoleRepository) {
        this.appRoleRepository = appRoleRepository;
    }
    
    @Override
    public AppRole creerRole(AppRole appRole) {
        return appRoleRepository.save(appRole);
    }

    @Override
    public AppRole mettreAJourRole(Long id, AppRole appRole) {
        return appRoleRepository.findById(id)
                .map(existingRole -> {
                    existingRole.setRolename(appRole.getRolename());
                    return appRoleRepository.save(existingRole);
                }).orElseThrow(() -> new RuntimeException("Rôle introuvable"));
    }

    @Override
    public void supprimerRole(Long id) {
        if (!appRoleRepository.existsById(id)) {
            throw new RuntimeException("Rôle introuvable");
        }
        appRoleRepository.deleteById(id);
    }

    @Override
    public List<AppRole> obtenirTousLesRoles() {
        return appRoleRepository.findAll();
    }

    @Override
    public AppRole obtenirRoleParId(Long id) {
        return appRoleRepository.findById(id)
               .orElseThrow(() -> new RuntimeException("Rôle introuvable"));
    }

    @Override
    public AppRole obtenirRoleParNom(String nomRole) {
        return appRoleRepository.findByRolename(nomRole);
               
    }
}
