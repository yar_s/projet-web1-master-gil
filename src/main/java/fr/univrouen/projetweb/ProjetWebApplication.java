package fr.univrouen.projetweb;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import fr.univrouen.projetweb.model.AppRole;
import fr.univrouen.projetweb.model.AppUser;
import fr.univrouen.projetweb.model.AppUserService;
import fr.univrouen.projetweb.model.AppRoleService;

@SpringBootApplication
public class ProjetWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetWebApplication.class, args);
    }

    @Bean
    CommandLineRunner start(AppUserService userService, AppRoleService roleService,
                            RepositoryRestConfiguration repositoryRestConfiguration) {
        repositoryRestConfiguration.exposeIdsFor(AppUser.class, AppRole.class);
        return args -> {
            // création et affichage des rôles
            AppRole role1 = new AppRole(null, "USER");
            AppRole role2 = new AppRole(null, "ADMIN");
            roleService.creerRole(role1);
            roleService.creerRole(role2);
            System.out.println("Rôle créé : " + role1);
            System.out.println("Rôle créé : " + role2);

            // création et affichage des utilisateurs
            AppUser ahmed = new AppUser(null, "ahmed", "123","ahmed@gmail.com", new ArrayList<>());
            AppUser yaroslav = new AppUser(null, "yaroslav", "456","yaroslav@gmail.com", new ArrayList<>());
            userService.ajouterUtilisateur(ahmed);
            userService.ajouterUtilisateur(yaroslav);
            System.out.println("Utilisateur créé : " + ahmed);
            System.out.println("Utilisateur créé : " + yaroslav);

            // ajout des rôles aux utilisateurs et affichage
            userService.ajouterRoleAUtilisateur("ahmed", "ADMIN");
            userService.ajouterRoleAUtilisateur("yaroslav", "USER");
            userService.ajouterRoleAUtilisateur("ahmed", "USER");
            AppUser updatedUser1 = userService.obtenirUtilisateurParNom("ahmed");
            AppUser updatedUser2 = userService.obtenirUtilisateurParNom("yaroslav");
            System.out.println("Utilisateur après ajout de rôle : " + updatedUser1);
            System.out.println("Utilisateur après ajout de rôle : " + updatedUser2);
            
            //affichage de tous les utilisateurs
            List<AppUser> users = userService.obtenirTousLesUtilisateurs();
            for (AppUser user : users) {
                System.out.println("Utilisateur: " + user.getUsername() + ", Rôles: " + user.getRoles());
            }
        };
    }
}
